! ---------------------------------------------------
! this program computes the geometry, analytical solution
! and compares the numeric and analytical solutions outputting
! the error versus mesh characteristi size. I spent a long time
! to understand this problem and come out with this solution.
! the references that were most useful are:
!
! Asher, Dynamics and Thermodynamics of Compressible Fluid Flow
! Hartmann, R. Adaptive Finite Element Methods for the Compressible
!              Euler Equations, Thesis.
!
! Note: use the included sed script (script_sed.bat) to format
! the geometry output files in a way ICEM can read them directly.
!
! The error norm is computed for density.
!
! Carlos Breviglieri Junior
! Apr 2008
!
! ---------------------------------------------------

module global

implicit none

integer :: nq, nk
real(16) :: q_top, k_max, k_min                    ! Geometry Parameters
real(16) :: gamma, gamm1                           ! Physical Parameters
real(16) :: eps                                    ! Computational Parameters

real(16), allocatable, dimension(:,:) :: xy


end module global


! ---------------------------------------------------

program ringleb_flow

use global

implicit none
	
	print *, 'Creates Ringleb curves geometry'
	
	call indat
		
	call geometry
	
	call output_geometry
	
	print *, 'Done.'
	
end program

! ---------------------------------------------------

subroutine indat

use global

implicit none

!! subsonic parameters
q_top = 0.35q0
k_min = 0.40q0
k_max = 0.60q0

! barth parameters
!q_top = 0.43q0
!k_min = 0.60q0
!k_max = 0.98q0

!wolf parameters
! q_top = 0.30q0
! k_min = 0.40q0
! k_max = 0.80q0

!! Wang ZJ parameters
!q_top = 0.50q0
!k_min = 0.70q0
!k_max = 1.20q0

!! Workshop parameters
! q_top = 0.50q0
! k_min = 0.70q0
! k_max = 1.50q0

! ! lazy parameters
! q_top = 0.2q0
! k_min = 0.2q0
! k_max = 0.4q0

gamma = 1.4q0
gamm1 = 0.4q0


nq = 500
nk = 500

eps = 1.0e-30

allocate (   xy(2,nk+nk+nq)   )
xy = 10.q0

end subroutine

! ---------------------------------------------------

subroutine geometry

use global

implicit none

integer :: i
real(16) :: q, k, step

! outter wall ( kmin )

q = q_top
step = abs(q_top-k_min)/real(nk-1,16)

do i=1, nk
	call xy_from_kq(xy(1,i), xy(2,i), k_min, q)
	q = q + step
enddo

! inner wall ( kmax )


q = q_top
step = abs(q_top-k_max)/real(nk-1,16)

do i=i, i+nk-1
	call xy_from_kq(xy(1,i), xy(2,i), k_max, q)
	q = q + step
enddo


! top boundary ( q constant )

k = k_min
step = abs(k_min-k_max)/real(nq-1,16)

do i=i, i+nq-1
	call xy_from_kq(xy(1,i), xy(2,i), k, q_top)
	k = k + step
enddo


return

end subroutine

! ---------------------------------------------------

subroutine output_geometry

use global

implicit none


integer :: i

open(10,file='geometry.dat')


do i = 1, nk+nk+nq
	write(10,115) xy(1,i), xy(2,i), 0.q0
enddo

close(10)


open(11,file='wall_out.dat')
do i = 1, nk
	write(11,115) xy(1,i), xy(2,i), 0.q0
enddo
do i = nk, 1, -1
	write(11,115) xy(1,i), -xy(2,i), 0.q0
enddo
close(11)

open(12,file='wall_in.dat')
do i = nk+1, nk+nk
	write(12,115) xy(1,i), xy(2,i), 0.q0
enddo
do i = nk+nk, nk+1, -1
	write(12,115) xy(1,i), -xy(2,i), 0.q0
enddo
close(12)

open(13,file='outlet.dat')
do i = nk+nk+1, nk+nk+nq
	write(13,115) xy(1,i), xy(2,i), 0.q0
enddo
close(13)

open(14,file='inlet.dat')
do i = nk+nk+1, nk+nk+nq
	write(14,115) xy(1,i), -xy(2,i), 0.q0
enddo
close(14)

115 format(f21.15,',',f21.15,',',f21.15)



return

end subroutine

! ---------------------------------------------------

 subroutine xy_from_kq(x, y, k, q)
 
 use global
 
 implicit none

 
! Input and Output
	real(16) :: q, k   ! Input
	real(16) :: x, y   ! Output
! Aux values
	real(16) :: q2, k2, coef, c, rho, J
	
	
	
	q2 = q**2.q0
	k2 = k**2.q0
	coef = q2 / k2
	c = sqrt( 1.q0 - (gamm1/2.q0)*q2 )
	rho = c**(2.q0/gamm1)
	J = (1.q0/c) + (1.q0/(3.q0*c**3.q0)) + (1.q0/(5.q0*c**5.q0)) - 0.5q0*log( (1.q0+c)/(1.q0-c) )
	
	
	x = (1.q0/(2.q0*rho))*( (1.q0/q2) - (2.q0/k2) ) + 0.5q0*J
	if ( (1.q0-coef) < 0.q0 ) then
! 		print *, " Triger"
		y = 0.q0   ! to avoid sqrt(negative) below.
	else
		y = (1.q0/(k*rho*q))*sqrt( 1.q0 - coef )
	endif

 end subroutine xy_from_kq
 
! ---------------------------------------------------

!  subroutine q_step(k, q)
!  
!  implicit none
!  
! !Input and output
! 	real(16) :: k, q_min, q_max     ! Input
! 	integer  :: j                   ! Input
! 	real(16) :: V, theta            ! Output
! !Local variables
! 	real(dp) :: th_min, th_max
! 	
! 	th_min = asin(q_top/k)
! 	th_max = asin(1.q0)
! 	theta = th_max + (th_min-th_max)/real(nk)*real(j-1)
! 	q = sin(theta)*k
! 	
!  end subroutine V_from_theta
 
 
 
 
 
 
 
 
 
 
 
 
 
 